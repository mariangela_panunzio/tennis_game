package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testFifteenThirty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Federer fifteen - Nadal thirty", status);
	}
	
	@Test
	public void testFortyThirty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Federer forty - Nadal thirty", status);
	}
	
	public void testPlayer1Wins() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Federer wins", status);
	}
	
	public void testFifteenForty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Federer fifteen - Nadal forty", status);
	}
	
	public void testDeuce() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Deuce", status);
	}
	
	public void testAdvantagePlayer1() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Advantage Federer", status);
	}
	
	public void testPlayer2Wins() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Nadal wins", status);
	}
	
	public void testAdvantagePlayer2() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Advantage Nadal", status);
	}
	
	public void testDeuceAfterAdvantage() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1=game.getPlayerName1();
		String playerName2=game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName2);
		
		String status=game.getGameStatus();
		
		//Act
		assertEquals("Deuce", status);
	}

}
